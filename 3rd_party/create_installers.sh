#!/bin/bash

if [[ $# != 1 ]]
then
    echo "============================================================"
    echo "USAGE: $(basename $0) <output directory>"
    echo "When the output directory does not exist, it will be created"
    echo "============================================================"
    exit
fi

output_dir=$1;

# Create output directory if it does not yet exist
mkdir -p $output_dir

# Package the component
pushd ../com_joomleague
echo Creating component zip-file...
  zip -r $output_dir/com_joomleague.zip . -x '*~'
popd

# Package the component languages
pushd ../languages/component
for lang in $(find . -maxdepth 1 -type d -regex './.*')
do
    pushd $lang
    language=$(echo $lang | sed "s/\.\///");
    zip -r $output_dir/com_joomleague-$language.zip . -x '*~'
    popd
done
popd

# Package the modules
pushd ../modules
for mod in $(find . -maxdepth 1 -type d -regex './mod_.*')
do
    pushd $mod
    module=$(echo $mod | sed "s/\.\///");
    zip -r $output_dir/$module.zip . -x '*~'
    popd
done
popd

# Package the modules languages
pushd ../languages/modules
for mod in $(find . -maxdepth 1 -type d -regex './mod_.*')
do
    pushd $mod
    module=$(echo $mod | sed "s/\.\///");
    for lang in $(find . -maxdepth 1 -type d -regex './.*')
    do
        pushd $lang
        language=$(echo $lang | sed "s/\.\///");
        zip -r $output_dir/$module-$language.zip . -x '*~'
        popd
    done
    popd
done
popd

# Package the plugins
pushd ../plugins
for plg in $(find . -maxdepth 1 -type d -regex './[a-z].*')
do
    pushd $plg
    plugin=$(echo $plg | sed "s/\.\///");
    zip -r $output_dir/plg_$plugin.zip . -x '*~'
    popd
done
popd

# -j = Junk path info
zip -j -r $output_dir/joomleague_bundled.zip $output_dir/com_*.zip $output_dir/mod_*.zip $output_dir/plg_*.zip

echo Installers were created
